from random import randint

def pole_trojkata(a, h):
    return (a * h) / 2

def tabliczka_mnozenia():
    for i in range(10):
        for j in range(10):
            print repr((i+1)*(j+1)).rjust(3),
        print


def ciag_fib(x):
    a,b = 1,1
    for i in range(x-1):
        a,b = b,a+b
        print a,


def zad_4():
    a = raw_input("Podaj znak")
    for i in a:
        if i.isdigit():
            print i,

def lotek():
    number_set = set()
    while len(number_set) < 6:
        number_set.add(randint(1,49))
    for i in number_set:
        print i,

def rzymskie(x):
    a = 0
    index = 0
    list_arab = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1]
    list_rzym = ["M", "CM", "D", "CD", "C","XC", "L", "XL", "X", "IX", "V", "IV", "I"]
    for i in range(int(len(list_arab))):
        while x[index:index+len(list_rzym[i])] == list_rzym[i]:
            a += list_arab[i]
            index += len(list_rzym[i])
    print a


def konwerter(system, liczba):
    if system == 'bin':
        print 'dec = ',
        print int(str(liczba), 2)
        print 'hex = ',
        print hex(int(str(liczba),2))

    if system == 'dec':
        print 'bin = ',
        print bin(liczba)
        print 'hex = ',
        print hex(liczba)

    if system == 'hex':
        print 'bin = ',
        print bin(liczba)
        print 'dec = ',
        print int(str(liczba),10)

